module UsersHelper
  # Returns the Gravatar for the given user.
  def gravatar_img(gravatar_id, options = { size: 80, alt: '' })
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, class: 'gravatar')
  end
end
