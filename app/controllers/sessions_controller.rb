class SessionsController < ApplicationController
  skip_before_action  :logged_in_user, only: [:new, :create]
  skip_after_action   :verify_authorized

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      approve_login(user)
    else
      deny_login
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

  private

  def approve_login(user)
    if user.activated?
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_back_or root_url
    else
      message  = 'Account not activated.'
      message += 'Check your email for the activation link.'
      flash[:warning] = message
      redirect_to root_url
    end
  end

  def deny_login
    flash.now[:danger] = 'Invalid email/password combination' # Not quite right!
    render 'new'
  end
end
