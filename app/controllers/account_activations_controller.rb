class AccountActivationsController < ApplicationController
  before_action :set_user, only: [:edit]
  skip_before_action  :logged_in_user
  skip_after_action   :verify_authorized

  def edit
    if @user && !@user.activated? && @user.authenticated?(:activation, params[:id])
      @user.activate
      log_in @user
      flash[:success] = 'Account activated!'
      redirect_to @user
    else
      flash[:danger] = 'Invalid activation link'
      redirect_to root_url
    end
  end

  private

  def set_user
    @user = User.find_by_email(params[:email])
  end
end
