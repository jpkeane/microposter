class MicropostsController < ApplicationController
  def create
    @micropost = current_user.microposts.build(micropost_params)
    authorize @micropost
    if @micropost.save
      flash[:success] = 'Micropost created!'
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @micropost = Micropost.find_by(id: params[:id])
    authorize @micropost
    @micropost.destroy
    flash[:success] = 'Micropost deleted'
    redirect_to root_url
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content, :picture)
  end
end
