class ApplicationController < ActionController::Base
  include Pundit
  include SessionsHelper

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protect_from_forgery with: :exception

  before_action :logged_in_user
  after_action :verify_authorized

  private

  # Confirms a logged-in user.
  def logged_in_user
    return if logged_in?
    store_location
    flash[:danger] = 'Please log in.'
    redirect_to login_url
  end

  def user_not_authorized
    flash[:danger] = 'You are not authorized to perform this action.'
    redirect_to((request.referer || root_path), status: :forbidden)
  end
end
