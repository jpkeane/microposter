class StaticPagesController < ApplicationController
  skip_before_action :logged_in_user
  skip_after_action :verify_authorized

  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def help
  end

  def about
  end

  def contact
  end
end
