class UsersController < ApplicationController
  skip_before_action  :logged_in_user,    only: [:new, :create, :show]
  skip_after_action   :verify_authorized, only: [:new, :create]
  before_action       :set_user,          only: [:show, :update, :edit, :destroy, :following, :followers]

  def index
    @users = User.where(activated: true).paginate(page: params[:page])
    authorize @users
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = 'Please check your email to activate your account.'
      redirect_to root_url
    else
      render 'new'
    end
  end

  def show
    authorize @user
    redirect_to root_url unless @user.activated?
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def edit
    authorize @user
  end

  def update
    authorize @user
    if @user.update_attributes(user_params)
      flash[:success] = 'Profile updated'
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    authorize @user
    @user.destroy
    flash[:success] = 'User deleted'
    redirect_to users_url
  end

  def following
    authorize @user
    @title = 'Following'
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    authorize @user
    @title = 'Followers'
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def set_user
    @user = User.find_by_id(params[:id])
  end

  # Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
