class MicropostPolicy < ApplicationPolicy
  attr_reader :user, :micropost

  def initialize(user, micropost)
    @user = user
    @micropost = micropost
  end

  def create?
    true
  end

  def destroy?
    @micropost.user_id == @user || @user.admin?
  end
end
