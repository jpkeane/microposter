class UserPolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def new?
    true
  end

  def index?
    true
  end

  def edit?
    @user == @record || @user.admin?
  end

  def update?
    @user == @record || @user.admin?
  end

  def destroy?
    @user.admin?
  end

  def show?
    true
  end

  def following?
    true
  end

  def followers?
    true
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
