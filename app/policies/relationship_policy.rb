class RelationshipPolicy < ApplicationPolicy
  attr_reader :user, :policy

  def initialize(user, policy)
    @user = user
    @policy = policy
  end

  def create?
    true
  end

  def destroy?
    true
  end
end
