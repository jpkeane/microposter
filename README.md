# Microposter

Twitter clone based on 
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## TODO
* Use Pundit for link deciding
* Move status feed to own module
* Add API

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

## Status
[![Codeship](https://img.shields.io/codeship/b20f96e0-67c6-0134-117f-5ade36a91ecb.svg?maxAge=2592000)](https://app.codeship.com/projects/176281)
[![Coverage Status](https://coveralls.io/repos/bitbucket/jpkeane/microposter/badge.svg?branch=master)](https://coveralls.io/bitbucket/jpkeane/microposter?branch=master)