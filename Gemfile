source 'https://rubygems.org'

ruby                              '2.3.1'
gem 'rails',                      '5.0.0.1'

# Datastores
gem 'mysql2',                     '0.4.4'
# gem 'redis', '~> 3.0'

# App servers
gem 'puma',                       '3.4.0'

# Core
gem 'bcrypt',                     '3.1.11'
gem 'will_paginate',              '3.1.0'
gem 'bootstrap-will_paginate',    '0.0.10'
gem 'pundit',                     '1.1.0'

# Uploads
gem 'fog',                        '1.38.0'
gem 'carrierwave',                '0.11.2'
gem 'mini_magick',                '4.5.1'

# Views
gem 'bootstrap-sass',             '3.3.6'
gem 'sass-rails',                 '5.0.6'
gem 'uglifier',                   '3.0.0'
gem 'coffee-rails',               '4.2.1'
gem 'jquery-rails',               '4.1.1'
gem 'turbolinks',                 '5.0.1'
gem 'jbuilder',                   '2.4.1'

# Seeders
gem 'faker',                    '1.6.3'

group :development do
  # Tooling
  gem 'web-console',              '3.1.1'
  gem 'listen',                   '3.0.8'
  gem 'spring',                   '1.7.2'
  gem 'spring-watcher-listen',    '2.0.0'
  gem 'better_errors',            '2.1.1'
  gem 'binding_of_caller',        '0.7.2'
  gem 'annotate',                 '2.7.1'
  gem 'letter_opener',            '1.4.1'
  
  # Static Code Analysis
  gem 'rubocop',                  '0.42.0'
  gem 'brakeman',                 '3.4.0'
end

group :development, :test do
  # Tooling
  gem 'byebug',                   '9.0.0', platform: :mri
  gem 'pry-rails',                '0.3.4'
  gem 'pry-byebug',               '3.4.0'
end

group :test do
  # Test frameworks
  gem 'rails-controller-testing', '0.1.1'
  gem 'minitest-reporters',       '1.1.9'
  gem 'shoulda-matchers',         '3.1.1'
  
  # Tooling
  gem 'coveralls',                '0.8.15', require: false
  gem 'guard',                    '2.13.0'
  gem 'guard-minitest',           '2.4.4'
end

group :production do
end
